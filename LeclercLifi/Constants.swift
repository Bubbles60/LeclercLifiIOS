struct Constants {
   static let baseURL = "http://api-ratp.pierre-grimaud.fr/v2/tramways/3b/stations/"
    //
    // MARK: Product group / Universe
    struct LeclercPrdGrpKrys {
        static let Parent = "parent"
        static let Child = "child"
        static let Level = "level"
        static let Left = "left"
        static let Top = "top"
    }
    
}
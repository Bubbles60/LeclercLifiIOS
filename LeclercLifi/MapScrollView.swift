//
//  MapScrollView.swift
//  LeclercLifi
//
//  Created by DFINLAY-AIR on 25/05/16.
//  Copyright © 2016 adentis.leclerc. All rights reserved.
//

import UIKit

class MapScrollView: UIScrollView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    

}

extension MapScrollView {
    func zoomToPoint(zoomPoint:CGPoint,var withScale scale:CGFloat,animated: Bool ) -> CGPoint {
        
        //Ensure scale is clamped to the scroll view's allowed zooming range
        scale = min(scale,self.minimumZoomScale);
        scale = max(scale,self.maximumZoomScale);
        //`zoomToRect` works on the assumption that the input frame is in relation
        //to the content view when zoomScale is 1.0
        //Work out in the current zoomScale, where on the contentView we are zooming
        var translatedZoomPoint = CGPointZero;
        translatedZoomPoint.x = zoomPoint.x + self.contentOffset.x;
        translatedZoomPoint.y = zoomPoint.y + self.contentOffset.y;
        
        //Figure out what zoom scale we need to get back to default 1.0f
        let zoomFactor = 1.0 / self.zoomScale;
        //By multiplying by the zoom factor, we get where we're zooming to, at scale 1.0f;
        translatedZoomPoint.x *= zoomFactor;
        translatedZoomPoint.y *= zoomFactor;
        
        //
        //work out the size of the rect to zoom to, and place it with the zoom point in the middle
        var destinationRect = CGRectZero;
        destinationRect.size.width = CGRectGetWidth(self.frame) / scale;
        destinationRect.size.height = CGRectGetHeight(self.frame) / scale;
        destinationRect.origin.x = translatedZoomPoint.x - (CGRectGetWidth(destinationRect) * 0.5);
        destinationRect.origin.y = translatedZoomPoint.y - (CGRectGetHeight(destinationRect) * 0.5);
        //
        zoomToRect(destinationRect, animated: false)
        //
        //return CGPoint(x: destinationRect.origin.x,y: destinationRect.origin.y)
        let a = (CGRectGetWidth(destinationRect) * 0.5)
        let b = (CGRectGetHeight(destinationRect) * 0.5)
        return CGPoint(x: a,y: b)
        
    }

    
   }
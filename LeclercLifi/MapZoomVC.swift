//
//  MapZoomVC.swift
//  LeclercLifi
//
//  Created by DFINLAY-AIR on 25/05/16.
//  Copyright © 2016 adentis.leclerc. All rights reserved.
//

import UIKit

class MapZoomVC: UIViewController {
    @IBOutlet weak var scrollView: MapScrollView!
    //
    @IBOutlet weak var imageViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageView: UIImageView!
    var photoName: String!
    @IBAction func screenTapped(sender: UITapGestureRecognizer) {
        let touchPoint = sender.locationInView(self.view)
        let newPinLoc = scrollView.zoomToPoint(touchPoint,withScale: 0.5,animated: false)
         print("Tapped")
       var image: UIImage = UIImage(named: "lecrec_pin_ns")!
       var bgImage = UIImageView(image: image)
        let screenBounds = scrollView.bounds
        
        bgImage.frame = CGRectMake((scrollView.bounds.size.width)/2,(scrollView.bounds.size.height)/2,15,20)
        
        scrollView.addSubview(bgImage)
        let a = CGFloat((scrollView.bounds.size.width)/2)
        let b =  CGFloat((scrollView.bounds.size.height)/2)
        
        bgImage.bounds.origin = CGPoint(x: a,y: b)
        
        //bgImage.frame = CGRectMake(screenBounds.origin.x+newPinLoc.x,screenBounds.origin.y+newPinLoc.y,15,20)
        
        //scrollView.addSubview(bgImage)

        //scrollView.bounds.origin
        print("x")
     }
    
    override func viewDidLoad() {
        //imageView.image = UIImage(named: photoName)
        let getProds = SetupProducts();
        getProds.readProducts()
    }
    
    
    private func updateMinZoomScaleForSize(size: CGSize) {
        // Calculate scale
        let widthScale = size.width / imageView.bounds.width
        let heightScale = size.height / imageView.bounds.height
        let minScale = min(widthScale, heightScale)
        //scrollView.minimumZoomScale = minScale
        //scrollView.zoomScale = minScale
        scrollView.minimumZoomScale = 0.55
        scrollView.maximumZoomScale = 1
        scrollView.zoomScale = 0.25
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateMinZoomScaleForSize(view.bounds.size)
        //
       
    }
    
    private func updateConstraintsForSize(size: CGSize) {
        
        let yOffset = max(0, (size.height - imageView.frame.height) / 2)
        imageViewTopConstraint.constant = yOffset
        imageViewBottomConstraint.constant = yOffset
        
        let xOffset = max(0, (size.width - imageView.frame.width) / 2)
        imageViewLeadingConstraint.constant = xOffset
        imageViewTrailingConstraint.constant = xOffset
        
        view.layoutIfNeeded()
    }
    
    
  
    

}
extension MapZoomVC: UIScrollViewDelegate {
    // Set image to scroll
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    func scrollViewDidZoom(scrollView: UIScrollView) {
        updateConstraintsForSize(view.bounds.size)
    }
    

}

//
//  TransportTVC.swift
//  LeclercLifi
//
//  Created by DFINLAY-AIR on 19/05/16.
//  Copyright © 2016 adentis.leclerc. All rights reserved.
//

import UIKit


class TransportTVC: UITableViewController {
    
    var allTimes = [String : [[String : AnyObject]]]()
    
    var allTimes2 = [AllRatpTimes]()
    
    
    
    //private var searches = [FlickrSearchResults]()
    //private let flickr = Flickr()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //let myTimes = RATPTimes()
        getTimes()
        print("Result \(allTimes)")
        
         // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return allTimes2.count
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return allTimes2[section].schedule.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        // Configure the cell...
        let val = allTimes2[indexPath.section].schedule[indexPath.row]
        let d = val["destination"]!
        cell.textLabel?.text = "\(val["destination"]!) en \(val["message"]!)"
        return cell
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Station \(allTimes2[section].stationDirection)"
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //==============================


    
    //==============================

}

extension TransportTVC {
    // MARK: Tramway Times
    func getTimes() {
        // create url Canal Saint-Denis --> Porte de la Chapelle
        getTimesForRoute("canal+saint+denis", direction: "porte+de+la+chapelle",stationdesc: "Canal Saint-Denis",directiondesc:"Porte de la Chapelle")
        getTimesForRoute("canal+saint+denis", direction: "porte+de+vincennes",stationdesc: "Canal Saint-Denis",directiondesc:"Porte de Vincennes")
        getTimesForRoute("rosa+parks", direction: "porte+de+la+chapelle",stationdesc: "Rosa Parks",directiondesc:"Porte de la Chapelle")
        getTimesForRoute("rosa+parks", direction: "porte+de+vincennes",stationdesc: "Rosa Parks",directiondesc:"Porte de Vincennes")
    }
    
    func getTimesForRoute(station:String,direction: String,stationdesc:String,directiondesc: String)
    {
        // if an error occurs, print it and re-enable the UI
        func displayError(error: String) {
            print(error)
            performUIUpdatesOnMain {
                //self.setUIEnabled(true)
                //self.photoTitleLabel.text = "No photo returned. Try again."
                //self.photoImageView.image = nil
            }
        }
        
        //let myUrlString = "\(Constants.baseURL)\(escapeParameters(station))?destination=\(escapeParameters(direction))"
        let myUrlString = "\(Constants.baseURL)\(station)?destination=\(direction)"
        let ratpURL = NSURL(string: myUrlString)!
        // create network request
        let task = NSURLSession.sharedSession().dataTaskWithURL(ratpURL) { (data, response, error) in
            
            /* GUARD: Was there an error? */
            guard (error == nil) else {
                displayError("There was an error with your request: \(error)")
                return
            }
            /* GUARD: Did we get a successful 2XX response? */
            guard let statusCode = (response as? NSHTTPURLResponse)?.statusCode where statusCode >= 200 && statusCode <= 299 else {
                displayError("Your request returned a status code other than 2xx!")
                return
            }
            
            /* GUARD: Was there any data returned? */
            guard let data = data else {
                displayError("No data was returned by the request!")
                return
            }
            var parsedResult:AnyObject!
            parsedResult="Default"
            do {
                parsedResult = try NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments)}
            catch {
                displayError("Could not parse json data")
            }
            /* GUARD: Is the "response" key in our result? */
            guard let timesDictionary = parsedResult["response"] as? [String : AnyObject] else {
                displayError("Cannot find key response in result")
                return
            }
            /* GUARD: Is the "response" key in our result? */
            guard let scheduleArray = timesDictionary["schedules"]  as? [[String : AnyObject]] else {
                displayError("Cannot find key schedules in response result")
                return
            }
            
            // Note "schedules" is a JSON array it starts with "[" and will return an Array but it is a list of json objects so it is and array of dictionarys
            print("First Item \(scheduleArray[0])")
            for schedule in scheduleArray {
            }
            //
            self.allTimes["\(stationdesc)"] = scheduleArray
            //
            let dets = AllRatpTimes(stationDirection: "\(stationdesc)",schedule: scheduleArray)
            self.allTimes2.append(dets)
            //
            performUIUpdatesOnMain({
                self.tableView.reloadData()
            })
            
            
        }
        task.resume()
        
        
        func escapeParameters (input: String) -> String{
            // Remove dangerous characters from sring for url
            if input.isEmpty {
                return ""
            }
            let escapedValue = input.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
            return escapedValue!
        }
    }
  }

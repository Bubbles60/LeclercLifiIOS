//
//  Universe+CoreDataProperties.swift
//  LeclercLifi
//
//  Created by DFINLAY-AIR on 17/06/16.
//  Copyright © 2016 adentis.leclerc. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Universe {

    @NSManaged var parent: String?
    @NSManaged var child: String?
    @NSManaged var level: String?
    @NSManaged var xpos: NSNumber?
    @NSManaged var ypos: NSNumber?

}

//
//  SetupProducts.swift
//  LeclercLifi
//
//  Created by DFINLAY-AIR on 17/06/16.
//  Copyright © 2016 adentis.leclerc. All rights reserved.
//

import Foundation
import UIKit
import CoreData


class SetupProducts {
    // MARK: Read json file from project
    func readProducts(){
        // Get managed object and set up Database
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        let managedObjectContext = appDelegate.managedObjectContext
        //
        let url = NSBundle.mainBundle().URLForResource("newuniverse", withExtension: "json")
        let data = NSData(contentsOfURL: url!)
        // Extract JSON object
        do {
            let object = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
            //
            guard let productData = object as? [[String: AnyObject]] else
            {
                print("Unable to parse data as a JSON array")
                return
            }
            
            for item in productData
            {
                updateDB(item, managedObjectContext: managedObjectContext)
            }
            
        }
        catch {
            print("Unable to parse data as json")
            return
        }
    }
    // MARK: Read Update Data base
    func updateDB(item : [String: AnyObject], managedObjectContext: NSManagedObjectContext ){
        //
        // Delete existing Data
        let entity =  NSEntityDescription.entityForName("Universe",
            inManagedObjectContext:managedObjectContext)
        let universe = Universe(entity: entity!,
            insertIntoManagedObjectContext: managedObjectContext)

        //
        
        universe.parent = (item[Constants.LeclercPrdGrpKrys.Parent] as? String)!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet()
        )
        universe.child = (item[Constants.LeclercPrdGrpKrys.Child] as? String)!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
        
        
        universe.level = item[Constants.LeclercPrdGrpKrys.Level] as? String
        universe.xpos = 0
        universe.ypos = 0
        if let x = item[Constants.LeclercPrdGrpKrys.Left]
        {
            let sx = item[Constants.LeclercPrdGrpKrys.Left] as? String
            if sx != nil{
                 universe.xpos = getIntPart(sx!)
            }
        }
        if let y = item[Constants.LeclercPrdGrpKrys.Top] {
            let sy = item[Constants.LeclercPrdGrpKrys.Left] as? String
            if sy != nil{
                universe.ypos =  getIntPart(sy!)
        }
        print("\(item[Constants.LeclercPrdGrpKrys.Parent]!) -  \(item[Constants.LeclercPrdGrpKrys.Child]!)  - \(item[Constants.LeclercPrdGrpKrys.Left]!)")
        do {
            try managedObjectContext.save()
        }
        catch{}

    }
    
    func getIntPart(mydoublestr:String) -> Int {
         let needle: Character = "."
         var myInt = ""
        if let idx = mydoublestr.characters.indexOf(needle) {
            let pos = mydoublestr.startIndex.distanceTo(idx)
             myInt = mydoublestr[mydoublestr.startIndex..<mydoublestr.startIndex.advancedBy(pos)]
        }
        else {
            myInt = "0"
        }
        let myI = Int(myInt)!
        return myI
    }
}

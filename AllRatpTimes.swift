//
//  AllRatpTimes.swift
//  LeclercLifi
//
//  Created by DFINLAY-AIR on 24/05/16.
//  Copyright © 2016 adentis.leclerc. All rights reserved.
//
import Foundation
struct AllRatpTimes {
    let stationDirection : String
    let schedule : [[String : AnyObject]]
}
